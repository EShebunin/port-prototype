import makeRequest from './makeRequest';

export async function getTableInfo() {
  const { data } = await makeRequest({
    url: '/doc',
    method: 'GET',
  });
  return data;
}

export async function getProperties(property) {
  const { data } = await makeRequest({
    url: `/properties/${property}`,
    method: 'GET',
  });

  return data;
}

export async function sendDocToBackend(objectDoc) {
  const { data } = await makeRequest({
    url: '/doc',
    method: 'PUT',
    data: objectDoc,
  });

  return data;
}
export async function sendChangeDocToBackend(idDoc, objectDoc) {
  const { data } = await makeRequest({
    url: `/doc/${idDoc}`,
    method: 'PATCH',
    data: objectDoc,
  });

  return data;
}
export async function deleteDocFromBackend(idDoc) {
  const { data } = await makeRequest({
    url: `/doc/${idDoc}`,
    method: 'DELETE',
  });

  return data;
}
