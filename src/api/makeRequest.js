import axios from 'axios';

/**
 * Функция для запросов на бек
 * TODO: сделать изящнее пока так =)
 * @param {Object} params
 * @param {String} params.url
 * @param {String} params.method
 * @param {Object} [params.data]
 * @param {Object} [params.params]
 * @param {Object} [params.headers]
 */
function makeRequest({ url, method, data = {}, params = {}, headers = {} }) {
  return axios({
    url: `https://air.nw-sys.ru/api/v1${url}`,
    // process.env.NODE_ENV === 'development'
    //   ? `http://localhost:5000/api${url}`
    //   : `https://air.nw-sys.ru/api/v1${url}`,
    method,
    params,
    data,
    headers,
  });
}

export default makeRequest;
