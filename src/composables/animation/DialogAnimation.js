export default function useDialogAnimation() {
  const dialogAnimation = {
    enter: 'duration-300 ease-out',
    'enter-from': 'opacity-0 scale-95',
    'enter-to': 'opacity-100 scale-100',
    leave: 'duration-200 ease-in',
    'leave-from': 'opacity-100 scale-100',
    'leave-to': 'opacity-0 scale-95',
  };

  const overlayAnimation = {
    enter: 'duration-300 ease-out',
    'enter-from': 'opacity-0',
    'enter-to': 'opacity-100',
    leave: 'duration-200 ease-in',
    'leave-from': 'opacity-100',
    'leave-to': 'opacity-0',
  };

  return {
    dialogAnimation,
    overlayAnimation,
  };
}
