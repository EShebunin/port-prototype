/**
 * Типы принимаемых грузов.
 * @readonly
 */
const shippingNames = Object.freeze({
  /** Трубы */
  pipes: 'трубы',
  /** Контейнер */
  crisper: 'контейнер',
  /** Геню груз */
  cargo: 'ген. груз',
});

export const formatEntitiesForPipes = (object) => ({
  name: object.name,
  inplace_count: object.inplace_count,
  pipe_tag: object.pipe_tag,
  weight: object.weight,
  height: object.height,
  diameter: object.diameter,
  thickness: object.thickness,
  segment_number: object.segment_number,
  place_number: object.place_number,
  extra: object.extra,
});

export default shippingNames;
