/** Vendor */
import { ref } from 'vue';
/** Api */
import { getTableInfo } from '@/api';

/**
 * Массив объектов всех документов
 * @type {import('@vue/reactivity').ToRef<Array<import('@/types/index').Document>>}
 */
const allDocuments = ref([]);

export default function useDocs() {
  /** Получение всех документов */
  const getAllDocuments = async () => {
    allDocuments.value = await getTableInfo();
  };

  return {
    allDocuments,
    getAllDocuments,
  };
}
