import { ref } from 'vue';

export default function useToogleModal() {
  /**
   * Флаг открытия модального окна для ввода табличной части
   * @type {import('@vue/reactivity').ToRef<Boolean>}
   * */
  const isOpenModal = ref(false);

  /**
   * Функция переключения состояния модального окна
   * @param {Boolean} value */
  const setIsOpenModal = (value) => {
    isOpenModal.value = value;
  };

  return {
    isOpenModal,
    setIsOpenModal,
  };
}
