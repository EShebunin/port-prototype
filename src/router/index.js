import { createWebHashHistory, createRouter } from 'vue-router';
import Home from '@/views/Home.vue';
import Acceptance from '@/views/Acceptance.vue';
import References from '@/views/References.vue';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/acceptance',
    name: 'Acceptance',
    component: Acceptance,
  },
  {
    path: '/references',
    name: 'References',
    component: References,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
