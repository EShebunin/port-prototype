export type Document = {
  /** id укрупненного названия номенклатуры */
  big: number
  contract: string
  danger_class: string
  // entities: [{…}]
  extra: string
  id: number
  object: string
  place: number
  port: number
  receive_date: string
  receiver: number
  send_date: string
  sender: number
  tag: string
  transport_tag: string
  transport_type: number
  type: number
}