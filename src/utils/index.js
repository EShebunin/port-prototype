/**
 * Возвращает объект из массива объектов по полю id
 * @param {Number|String} id
 * @param {Array<{id: Number|String}>} array
 * @return {{id: Number|String}}
 */
export function getPropertiesValue(id, array) {
  return array.find((item) => item.id === id);
}

export default {
  getPropertiesValue,
};
